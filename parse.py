import inspect

class NodeObj:
    pass
    # This object contains a dict, _epsilon, whose keys are the location of a nullable symbol that has
    # been eliminated in the preprocessing, and values are the objects corresponding to this symbol.

class CFGNonterm:
    def __init__(self, productions):
        # the list of all productions of this nonterminal
        # the element is a list of strings that contains all the elements.
        # An empty list means epilson production
        self.productions = productions
        # A list of code that initializes the object corresponding to
        # the nullable symbols in the productions list. 
        self.epsilonInit = []

class CFG:
    def __init__(self, nonterms, terms, actions, start):
        # A dict of all nonterminals and their production rules:
        # -  value: a list of production rules that can be used to deduce this nonterminal.
        #   + production rule: a list of strings where each string is 
        #     the name of a nonterminal, terminal or action.
        self.nonterms = nonterms
        # A list of all terminals type name. 
        self.terms = terms
        # A dict of action names and corresponding pairs of actions.
        # - action 1: a function that takes the following parameters, in order:
        #   + a parameter that represent the attributes of the symbol itself.
        #   + a list of parameters that matches all the symbol ON THE LEFT of the action.
        #   (This action should only read from the list of symbols.)
        # - action 2: None, or a function that takes the following parameters, in order:
        #   + a parameter that holds the return value from action 1. 
        #   + a list of parameters that matches all the symbol ON THE RIGHT of the action.
        #   (This action should only write to the list of symbols.)
        # - All parameters must not be skipped. If it is not used, simply use _ to replace them.
        self.actions = actions
        # The start nonterminal. 
        self.start = start

# Helper function: remove all None in the list
def removeNone(l):
    return list(filter(lambda x: x is not None, l))

# Helper function: remove all action node in the production during FIRST and FOLLOW building.
def removeAction(cfg, prod):
    newProd = []
    for s in prod:
        if s not in cfg.actions:
            newProd.append(s)
    return newProd

# Helper function: combine the synthesis extraction and inheritance assignement together.
def combineAction(action):
    argLen = len(inspect.getargspec(action[0])[0]) - 1 # without "this"
    if action[1]:
        return (lambda act, alen: lambda this, *args: act[1](act[0](this, *args[:alen]), *args[alen:]))(action, argLen)
    else:
        return (lambda act, alen: lambda this, *args: act[0](this, *args[:alen]))(action, argLen)

# Remove useless production
def removeUseless(cfg):
    # Check all the productions and see if they can generate some terminals
    # Anything in unusedNonterm has no production that generates terminals
    # discovered at this time.
    # When it becomes
    unusedNonterm = None
    newUnusedNonterm = set(cfg.nonterms.keys())
    while unusedNonterm != newUnusedNonterm:
        unusedNonterm = newUnusedNonterm.copy()
        for nonterm in unusedNonterm:
            # To be considered used, every symbol in the production must be
            # generatable. The nonterminal is considered generatable if there
            # is at least one used production.
            for prod in cfg.nonterms[nonterm].productions:
                used = False
                for symbol in prod:
                    if symbol not in unusedNonterm and symbol not in cfg.actions:
                        used = True
                if used:
                    newUnusedNonterm.remove(nonterm)
                    break
    # Remove ungeneratable symbol from the CFGNonterm dict...
    for symbol in unusedNonterm:
        del cfg.nonterms[symbol]
    # ...and any production mentions them from the list.
    for nonterm in cfg.nonterms:
        newProductions = list()
        for prod in cfg.nonterms[nonterm].productions:
            keep = True
            for symbol in prod:
                if symbol in unusedNonterm:
                    keep = False
                    break
            if keep:
                newProductions.append(prod)
        cfg.nonterms[nonterm].productions = newProductions
    # Now, search for any unreachable symbol.
    reachable = None
    newReachable = set(cfg.start)
    while reachable != newReachable:
        reachable = newReachable
        for nonterm in reachable:
            for prod in cfg.nonterms[nonterm].productions:
                for symbol in prod:
                    reachable.add(symbol)
    # Remove all symbols, nonterminals and terminals and actions, that are not in the set.
    newNonterms = dict()
    newTerms = list()
    newActions = dict()
    for nonterm in cfg.nonterms:
        if nonterm in reachable:
            newNonterms[nonterm] = cfg.nonterms[nonterm]
    for term in cfg.terms:
        if term in reachable:
            newTerms.append(term)
    for action in cfg.actions:
        if action in reachable:
            newActions[action] = cfg.actions[action]
    return CFG(newNonterms, newTerms, newActions, cfg.start)

# helper function: translate the action arguments
def argTranslate(mapping, since, func, *args):
    return func(tuple(map(lambda i: args[i - since] if i > 0 else args[0].__placeholder__[-i], mapping[since:])))

# Remove epsilon production
# Warning: this algorithm cannot process ambiguous CFG
def removeEpsilon(cfg):
    # Build nullable
    # The key is the name of nullable symbols, and the value is the "null action".
    nullable = dict()
    newNonnullable = set()
    # This loop detect all nonterminals that contains epsilon productions.
    # We will build a composite function to combine the action in the nullable production. 
    for nonterm in cfg.nonterms:
        for prod in cfg.nonterms[nonterm].productions:
            epsilon = True
            for s in prod:
                if s in cfg.nonterms or s in cfg.terms:
                    epsilon = False
            if epsilon:
                if prod:
                    # if there is multiple action in the list, combine them together.
                    actionList = map(combineAction, map(lambda x: cfg.actions[x], prod))
                    # Combine them into a null action
                    nullable[nonterm] = (lambda actList: lambda this, *args: map(lambda action: action(this, *args), actList))(actionList)
                else:
                    nullable[nonterm] = None
        if nonterm not in nullable:
            newNonnullable.add(nonterm)
    nonnullable = set()
    # This loop detects all nonterminals that can deduce to a empty string. 
    while newNonnullable != nonnullable:
        nonnullable = newNonnullable
        newNonnullable = set()
        for nonterm in nonnullable:
            for prod in cfg.nonterms[nonterm].productions:
                epsilon = True
                for s in prod:
                    if s not in nullable:
                        epsilon = False
                if epsilon:
                    # Combine the null action of the nullable nonterminals in this production
                    actionList = map(lambda x: nullable[x], prod)
                    # Combine them into a null action
                    nullable[nonterm] = (lambda actList: lambda this, *args: map(lambda action: action(this, *args), actList))(actionList)
            if nonterm not in nullable:
                newNonnullable.add(nonterm)
    # put null action to the list
    for act in nullable:
        if nullable[act]:
            cfg.actions["__null__" + act] = nullable[act]
    # create new production rules with zero or more nullable symbol removed.
    # To ensure the parameter can be passed correctly
    for nonterm in cfg.nonterms:
        nontermProdSet = []
        productionCount = 0
        for prod in cfg.nonterms[nonterm].productions:
            prodSet = list()
            # This variable is a list of pair of the new production and a list of indices.
            # The list is a translation table which is used during the execution of actions.
            # The wrapper function will first read the arguments based on this list - 
            # if the number at ith slot is positive, the ith argument passed to the action
            # will be read from the nonnullable objects that survive this process and are passed
            # by the engine. If it is nonpositive, it is negated and then indexes the list of "nullable"
            # object removed from the production.
            # The third element of the tuple is the size of the nullable object in this production.
            # It will be used to index the next available slot in the list of nullable placeholder object.
            newProdSet = [(prod, dict(zip(range(1, len(prod) + 1))), 0)]
            # It produce 2^n productions in the worse case, since every nullable symbol result in two productions
            # - one has it removed, and another leaves it alone.
            for i in range(len(prod)):
                if prod[i] in nullable:
                    prodSet = newProdSet
                    newProdSet = list()
                    for p in prodSet:
                        # Put the original production into the new list
                        newProdSet.append(p)
                        # For every symbol removed, modify the list entry
                        newIndexList = p[1].copy()
                        newIndexList[i] = -p[2] # See above - the negative number indicate a value in placeholder object
                        # Insert the null action
                        newProd = p[0].copy()
                        if nullable[prod[i]]:
                            newProd[i] = "__null__" + act
                        else:
                            newProd[i] = None
                        newProdSet.append((newProd, newIndexList, p[2] + 1))
            for p in newProdSet:
                # For every production, append an action that create the placeholder objects for removed nullable symbols.
                newProd = ["__null_placeholder_" + str(productionCount) + "__"]
                productionCount += 1
                # Then, for every action in the production, wrapped them with the new action
                for s in p:
                    if s in cfg.actions:
                        newProd.append("__wrapped_" + s + "__")
                    else:
                        newProd.append(s)
        cfg.nonterms[nonterm].productions = nontermProdSet
    return cfg

# Remove left recursion and loop
def removeLeftRecursion(cfg):
    # Number the nonterminal
    nontermList = list(cfg.keys())
    for i in range(len(nontermList)):
        newRecProd = []
        newNonRecProd = []
        newNonterm = []
        # Start inner loop to purge
        if i != 0:
            replaceNonterm = set(nontermList[0:i - 1])
            # Inner loop to remove non-immediate left recursion
            for prod in cfg[nontermList[i]].productions:
                if prod[0] in replaceNonterm:
                    for replaceProd in cfg[prod[0]].productions:
                        if replaceProd[0] == nontermList[i]:
                            newRecProd = newRecProd.append(replaceProd + prod[1:])
                        else:
                            newNonRecProd = newNonRecProd.append(replaceProd + prod[1:])
                else:
                    if prod[0] == nontermList[i]:
                        newRecProd = newRecProd.append(prod)
                    else:
                        newNonRecProd = newNonRecProd.append(prod)
        # Remove immediate left recursion and loop
        newRecProd = filter(lambda prod: prod != [nontermList[i]], newRecProd)
        newNonterm.productions = \
            map(lambda prod: prod[1:].append(nontermList[i] + "\n"), newRecProd)
        cfg[nontermList[i]].productions = newNonterm.productions.copy()
        cfg[nontermList[i] + "\n"] = newNonterm
    return cfg

# ===================

# helper function: group by the ith character
def groupProdBy(prods, i):
    grouping = dict()
    standalonePrefix = False
    for prod in prods:
        if i == len(prod):
            # The prefix can stand alone in the productions list
            standalonePrefix = True
        elif prod[i] not in grouping:
            grouping[prod[i]] = list()
        grouping[prod[i]].append(prod[i:])
    return list(grouping.values())

# Merge common coefficient
def removeCommonCoeff(cfg):
    newNonterms = dict()
    # Create partition for every nonterminal
    for nonterm in cfg.nonterms:
        # This function will treat those prefix with identical symbols but different
        # Grammar action as the same prefix. To deal with the different grammar actions
        # case, we delay the grammar actions until we can determine which production this
        # is. 
        # We create a list of stack to hold these element. 
        actionsStack = []
        coeff = set()
        part = None
        newPart = map(lambda g: (g, []), groupProdBy(nonterm, 0))
        while part != newPart:
            part = newPart
            newPart = set()
            for group in newPart:
                # Only one element, no need to divide
                if len(group[0]) == 1:
                    continue
                i = 0
                while i < len(group[0][0]):
                    nextChar = group[0][0][i]
                    for prod in group[0]:
                        if prod[i] != nextChar:
                            nextChar = None # Flag to break outer loop
                            break
                    if nextChar == None:
                        break
                    i += 1
                # Now i is the end index of the longeset prefix
                # Group by prod[i] symbol
                grouping = groupProdBy(group[0], i)
                # Create prefix and new partition
                coeff.add(group[0][0][:i])
                for g in grouping:
                    newPart.add((g, group[1].copy().append(group[0][0][:i])))
        # Name each prefix
        coeffDict = dict()
        i = 0
        for c in coeff:
            name = nonterm + "\n" + str(i)
            coeffDict[c] = name
            newNonterms[name] = c
        # And replace the prefix in the partition
        newProds = list()
        for group in part:
            newProd = list()
            for prefix in group[1]:
                newProd.append(coeffDict[prefix])
            newProd += group[0]
            newProds.append(newProd)
        newNonterms[nonterm] = CFGNonterm(newProds)
    cfg.nonterms = newNonterms
    return cfg

# helper function: resolve dict
def dictResolve(workingSet, res):
    newWorkingSet = dict()
    while len(workingSet) != 0:
        for nonterm in workingSet:
            newSymbols = set()
            for symbol in workingSet[nonterm]:
                if symbol not in workingSet:
                    if symbol: # Skip "" that denote epsilon
                        res[nonterm] = res[nonterm].union(res[symbol])
                    else:
                        res[nonterm].add(symbol)
                else:
                    # Skip this
                    newSymbols.add(symbol)
            if len(newSymbols) != 0:
                newWorkingSet[nonterm] = newSymbols
        if workingSet == newWorkingSet:
            # All symbols are skipped - there is a left-recursive
            raise Exception("Loop of left-recursion - please apply the removal process")
        workingSet = newWorkingSet
        newWorkingSet = dict()
    return res

# Construct lookahead table
def buildTable(cfg):
    # Build FIRST
    # Put all terminals into the FIRST function (map to themself)
    first = dict(map(lambda a: (a, set([a])), cfg.terms))
    workingSet = dict()
    # If the first elements of a nonterminal's productions are terminal, put them
    # into the FIRST function
    # Otherwise, put them into the new working set
    for nonterm in cfg.nonterms:
        first[nonterm] = set()
        unresolvedNonterm = set()
        for prod in cfg.nonterms[nonterm]:
            prod = removeAction(cfg, prod)
            if prod == []: # If this is an epsilon production, put an "epsilon" symbol into FIRST
                first[nonterm].add("")
            elif prod[0] in cfg.terms:
                first[nonterm].add(prod[0])
            else:
                unresolvedNonterm.add(prod[0])
        if unresolvedNonterm:
            workingSet[nonterm] = unresolvedNonterm
    # Then, apply the same action to the nonterminals in the working set
    # until everything is removed from the working set
    first = dictResolve(workingSet, first)
    # Build FOLLOW
    follow = dict(map(lambda symbol: (symbol, set()), cfg.nonterms.keys()))
    follow[cfg.start].add("$")
    workingSet = dict(map(lambda symbol: (symbol, set()), cfg.nonterms.keys()))
    del workingSet[cfg.start]
    # Go throw all productions
    for nonterm in cfg.nonterms:
        for prod in cfg.nonterms[nonterm]:
            prod = removeAction(cfg, prod)
            # empty symbol stack: at the end of the loop, they will be added to working set
            # Though it also includes the last non-empty symbol
            emptyStack = []
            for symbol in prod:
                if "" not in first[symbol]:
                    # Clear the stack, add FIRST of the current symbol to FOLLOW of all symbols in the stack
                    while emptyStack:
                        top = emptyStack.pop()
                        follow[top] = follow[top].union(first[symbol])
                else:
                    # Otherwise, don't clear the stack, but go through every element and add FIRST of the current
                    # to their FOLLOW
                    for top in emptyStack:
                        follow[top] = follow[top].union(first[symbol])
                        follow[top].remove("")
                # In both case, push the current back unless it is a terminal
                if symbol not in cfg.terms:
                    emptyStack.append(symbol)
            # Pop everything out and put them into the working set
            while emptyStack:
                top = emptyStack.pop()
                workingSet[top].add(nonterm)
    # Purge the nonterminals that are in the working set of themselves
    # and also empty set
    newWorkingSet = dict()
    for nonterm in workingSet:
        if nonterm in workingSet[nonterm]:
            workingSet[nonterm].remove(nonterm)
        if workingSet[nonterm]:
            newWorkingSet[nonterm] = workingSet[nonterm]
    workingSet = newWorkingSet
    # Resolve it in the manner of FIRST
    follow = dictResolve(workingSet, follow)
    # The nonterminal and terminal index tables
    i = 0
    nontermInd = dict()
    for nonterm in cfg.nonterms:
        nontermInd[nonterm] = i
        i += 1
    j = 1 # For there is "$"
    termInd = dict()
    termInd["$"] = 0
    for term in cfg.terms:
        termInd[term] = j
        j += 1
    # build the index table
    tranTable = [None] * (i * j)
    # The helpder indexing function
    indFunc = lambda x, y: nontermInd[x] * j + termInd[y]
    for nonterm in cfg.nonterms:
        for prod in cfg.nonterms[nonterm]:
            prodR = removeAction(cfg, prod)
            if prodR != []:
                for symbol in first[prodR[0]]:
                    if symbol == "":
                        # Epsilon in FIRST: use FOLLOW instead
                        for sym in follow[prodR[0]]:
                            tranTable[indFunc(nonterm, sym)] = prod
                    else: # Otherwise, just add it into the table
                        tranTable[indFunc(nonterm, symbol)] = prod
            else:
                # A epsilon production, add FOLLOW(nonterm) to it
                for sym in follow[nonterm]:
                    tranTable[indFunc(nonterm, sym)] = prod
    # The returned function
    def deduceNonterm(currentNonterm, inputTerm, tranTable=tranTable):
        res = tranTable[indFunc(currentNonterm, inputTerm)]
        if res == None:
            raise Exception("Error when parsing " + inputTerm)
        return res
    return deduceNonterm

# Entrance function
def getParser(cfg):
    deduceFunc = buildTable(cfg)
    def run(stream, filterFunc=lambda _: True, cfg=cfg, deduceFunc=deduceFunc):
        return deduce(cfg, stream, deduceFunc, filterFunc)
    return run

def deduce(cfg, stream, deduceFunc, filterFunc):
    stack = []
    # Stack info
    currentProd = [cfg.start]
    pointer = 0
    currentObj = [NodeObj()]
    parentObj = None
    # filter the stream with the provided function
    stream = list(filter(filterFunc, stream))
    # Add a "$" to the end of the stream to mark EOF
    stream.append(("$", None)) 
    for lex in stream:
        moveToNext = False
        while not moveToNext:
            if currentProd[pointer] in cfg.nonterms:
                # Nonterminal, deduce it
                stack.append((pointer, currentProd, currentObj, parentObj))
                parentObj = currentObj[pointer]
                currentProd = deduceFunc(currentProd[pointer], lex[0])
                currentObj = list(map(lambda s: None if s in cfg.actions else NodeObj(), currentProd))
                pointer = 0
            elif currentProd[pointer] in cfg.terms:
                # Terminal (always match the lex), put the attribute in, then break from the loop
                currentObj[pointer].v = lex[1]
                pointer += 1
                moveToNext = True
            else:
                # Execute grammer action
                synthesis, inherited = cfg.actions[currentProd[pointer]]
                if synthesis:
                    synRes = synthesis(parentObj, *tuple(removeNone(currentObj[0:pointer])))
                else:
                    synRes = None
                pointer += 1
                if inherited:
                    inherited(synRes, *tuple(removeNone(currentObj[pointer:])))
            # Pop if reach the end of current productions
            while pointer == len(currentProd):
                if len(stack) == 0:
                    # Finished deducing
                    moveToNext = True
                    break
                pointer, currentProd, currentObj, parentObj = stack.pop()
                pointer += 1
    return currentObj[0]