import functools
import operator

# The format of regular expression used here:
# 1. There are seven reserved characters with special meaning: ()[]|*\+?
# if any of these characters need to appear in the string, \ must precede them.
# 2. Any user-specified character set is represented by "[set-name]", where set-name
# is specified in the dictionary. Do not use a special character (though only ']'
# in the set-name will cause problem)
# 3. Any character not reserved and not in the set-name will be treated as a
# single-character set which match itself in the input string.
# 4. Every two adjancent characters set, if not separated by '|', will
# be treated as if there is a "concatenate" operator between them.
# 5. * is an unary operator (closure) and only affect the element preceding it.
# 6. No space or dollar sign is allowed for symbol name.

class RegExpElement:
    pass

class CharSet(RegExpElement):
    def __init__(self, matchFunc, name):
        self.matchFunc = matchFunc
        self.name = name
    def check(self, c):
        return self.matchFunc(c)
    def __repr__(self):
        return "CharSet(" + self.name + ")"
    def __hash__(self):
        return hash(self.name)
    def __eq__(self, other):
        if other is None:
            return False
        return self.name == other.name

class LeftPar(RegExpElement):
    def __init__(self):
        pass
    def __repr__(self):
        return "LeftPar"

class RightPar(RegExpElement):
    def __init__(self):
        pass
    def __repr__(self):
        return "RightPar"

class Concatenate(RegExpElement):
    def __init__(self):
        pass
    def __repr__(self):
        return "Concatenate"

class Alternate(RegExpElement):
    def __init__(self):
        pass
    def __repr__(self):
        return "Alternate"

class Closure(RegExpElement):
    def __init__(self):
        pass
    def __repr__(self):
        return "Closure"

class OneOrMore(RegExpElement):
    def __init__(self):
        pass
    def __repr__(self):
        return "OneOrMore"

class ZeroOrOne(RegExpElement):
    def __init__(self):
        pass
    def __repr__(self):
        return "ZeroOrMore"

# The function parsing the regular expressions
def parseRegex(regexp, charsetDict):
    # first, get it into lexical elements
    lexList = []
    setName = None
    escaped = False
    
    # A utility function to insert concatenate
    def addConcatenate():
        if len(lexList) != 0:
            if not isinstance(lexList[len(lexList) - 1], LeftPar) and not isinstance(lexList[len(lexList) - 1], Alternate):
                lexList.append(Concatenate())
                
    for c in regexp:
        if escaped:
            escaped = False
            # This charset is not following another operator or left parentheses
            addConcatenate()
            lexList.append(CharSet(lambda x, c=c: x == c, "\\" + c))
        elif setName != None:
            if c != ']':
                setName += c
            else:
                addConcatenate()
                lexList.append(CharSet(charsetDict[setName], setName))
                setName = None
        elif c == '[':
            setName = str()
        elif c == '*':
            lexList.append(Closure())
        elif c == '|':
            lexList.append(Alternate())
        elif c == '(':
            addConcatenate()
            lexList.append(LeftPar())
        elif c == ')':
            lexList.append(RightPar())
        elif c == '+':
            lexList.append(OneOrMore())
        elif c == '?':
            lexList.append(ZeroOrOne())
        elif c == '\\':
            escaped = True
        else:
            addConcatenate()
            lexList.append(CharSet(lambda x, c=c: x == c, "\\" + c))
    # Then, convert it to postfix
    res = []
    stack = []
    for lex in lexList:
        if type(lex) is LeftPar:
            stack.append(lex)
        elif type(lex) is RightPar:
            reachBottom = True
            while len(stack) != 0:
                top = stack.pop()
                if type(top) is LeftPar:
                    reachBottom = False
                    break
                res.append(top)
            if reachBottom:
                # Unpaired right parentheses
                raise Exception("Unpaired Right Parentheses")
        elif type(lex) is CharSet:
            res.append(lex)
        elif type(lex) is Closure or type(lex) is OneOrMore or type(lex) is ZeroOrOne:
            res.append(lex)
        elif type(lex) is Concatenate:
            stack.append(lex)
        elif type(lex) is Alternate:
            # Pop all concatenate in the stack until getting a bracket or bottom
            while len(stack) != 0:
                top = stack.pop()
                if type(top) is LeftPar:
                    stack.append(top)
                    break
                res.append(top)
            stack.append(lex)
    # Pop the rest out
    while len(stack) != 0:
        top = stack.pop()
        if type(top) is LeftPar:
            # parentheses mismatch
            raise Exception("Unpaired Left parentheses")
        res.append(top)
    return res

# Finite sate machine (FSM) representation in linked list
class FSMState:
    # next available id
    nextID = 0
    # Reset nextID
    @staticmethod
    def resetID():
        FSMState.nextID = 0
    def __init__(self):
        self.epsilonNext = [] # a list of all reachable state with null input
        self.next = dict() # a dict containing input character and corresponding next state
        self.accepted = False # indicate if this is an accepted state
        self.acceptAction = None # run when this state is attained
        self.ruleName = str() # the name of the rule - only valid for accepted states
        self.number = FSMState.nextID # For debug purpose, distinguish different states
        FSMState.nextID += 1 
    def run(self, char):
        if self.epsilonNext:
            raise Exception("Nondeterministic FSM cannot run")
        # It returns a set of FSMs because character set can overlap
        match = map(lambda n: (n.matchFunc(char), self.next[n]), self.next)
        match = filter(lambda res: res[0], match)
        return map(lambda res: res[1], match)
    def description(self):
        return "FSMState(" + str(self.number) + "), "
    def __hash__(self):
        return self.number
    def __eq__(self, other):
        return self.number == other.number
    def __str__(self):
        return self.__repr__()
    def __repr__(self):
        # helper
        def tranPrinter(tran):
            return tran.name + " : " + self.next[tran].description() + ", "
        res = self.description() + "epsilon{" + functools.reduce(operator.add, map(FSMState.description, self.epsilonNext), "") + "}\n"\
            + "tran{" + functools.reduce(operator.add, map(tranPrinter, self.next), "") + "}\n"
        return res

# The function that build nondeterministic finite state machine
def toNFSM(name, rule):
    # Create an entrance state
    #head = FSMState()
    #tail = head
    # And push it to the stack
    stateStack = []
    #stateStack.append((head, tail))
    # Now, start reading the postfix expression
    try:
        for element in rule.regexp:
            if type(element) is CharSet:
                # Create a simple transition
                head = FSMState()
                tail = FSMState()
                head.next[element] = tail
                stateStack.append((head, tail))
            elif type(element) is Concatenate:
                head, tail = stateStack.pop()
                prevHead, prevTail = stateStack.pop()
                prevTail.epsilonNext.append(head)
                stateStack.append((prevHead, tail))
            elif type(element) is Alternate:
                head1, tail1 = stateStack.pop()
                head2, tail2 = stateStack.pop()
                head = FSMState()
                tail = FSMState()
                head.epsilonNext.append(head1)
                head.epsilonNext.append(head2)
                tail1.epsilonNext.append(tail)
                tail2.epsilonNext.append(tail)
                stateStack.append((head, tail))
            elif type(element) is Closure:
                head, tail = stateStack.pop()
                newHead = FSMState()
                newTail = FSMState()
                newHead.epsilonNext.append(head)
                newHead.epsilonNext.append(newTail)
                tail.epsilonNext.append(head)
                tail.epsilonNext.append(newTail)
                stateStack.append((newHead, newTail))
            elif type(element) is OneOrMore:
                head, tail = stateStack.pop()
                tail.epsilonNext.append(head)
                stateStack.append((head, tail))
            elif type(element) is ZeroOrOne:
                head, tail = stateStack.pop()
                head.epsilonNext.append(tail)
                stateStack.append((head, tail))
    except IndexError:
        raise Exception("Operator missing operand")
    # Set accepted states
    head, tail = stateStack.pop()
    tail.accepted = True
    tail.acceptAction = rule.acceptAction
    tail.ruleName = name
    return head

# +++++++++++++++++++++++++++++

# The function that convert NFSM to DFSM (deterministic FSM) using subset construction
# inputState: A SET of the NFSM state.
def toDFSM(inputState, markedState=None):
    if markedState == None:
        markedState = dict()
    nfsmStateSet = frozenset(functools.reduce(set.union, map(lambda s: epilsonClosure(s, set()), inputState), set()))
    # If this has already been marked, return the corresponding DFSM state
    # otherwise, mark this state
    if nfsmStateSet in markedState:
        return markedState[nfsmStateSet]
    res = FSMState()
    res.accepted = False
    markedState[nfsmStateSet] = res
    # Check if this is a accepted states; if yes, copy the accepted action over
    acceptedName = None
    for s in nfsmStateSet:
        if s.accepted:
            if acceptedName:
                raise Exception("Ambiguous lexical rules definition: <" + acceptedName + ">, <" + s.ruleName + ">")
            res.accepted = True
            res.acceptAction = s.acceptAction
            res.ruleName = s.ruleName
            acceptedName = s.ruleName

    # Merge all states reachable with one character in the given the same character set
    tranDict = dict()
    for nfsmState in nfsmStateSet:
        for charset in nfsmState.next:
            if charset not in tranDict:
                tranDict[charset] = set()
            tranDict[charset].add(nfsmState.next[charset])

    # Now we have a list of all character sets that can move the FSM to the
    # next state. Create a transition for them.
    for charset in tranDict:
        res.next[charset] = toDFSM(tranDict[charset], markedState)
    return res

# helper function for toDFSM: calculate epilson-closure
# fsmNode: A state (NOT A COLLECTION OF SETS) whose e-closure need to be calculated
def epilsonClosure(fsmNode, closureSet=None):
    if closureSet == None:
        closureSet = set()
    closureSet.add(fsmNode)
    for n in fsmNode.epsilonNext:
        if n not in closureSet:
            epilsonClosure(n, closureSet)
    return closureSet

# +++++++++++++++++++++++++++++++++++

# The DFSM output from subset construction is not necessarily with the minimal states.
# This function will optimize the DFSM and reduce the states.
def optimizeDFSM(dfsm):
    # Assign all states to some groups; for the initial set, every accepted state
    # is in its own groups, and all other states is in a single group (marked 0).
    # The set marked as -1 is the dead state -- if this character set appears,
    # compilation error
    states, charset = allStatesAndCharset(dfsm)
    states = list(states)
    charset = list(charset)
    i = 1
    newPartition = dict()
    for state in states:
        if state.accepted:
            newPartition[state] = i
            i += 1
        else:
            newPartition[state] = 0
    # Keep partitioning all these states until all states in one group will move
    # to the states in the same group when given a character in a charset.
    partition = None
    tranDict = None
    while newPartition != partition:
        partition = newPartition
        newPartition = dict()
        # Next available id for new partition group
        i = 0
        # A dictionary whose:
        # - keys are a pair of integer and list where:
        #   + integer: the index of the current group in the old partition.
        #   + list: the i-th elements in which represent the groups (in the old partition) it will
        #     go after getting a character in charset[i].
        # - values are the index for the group in the new partition whose elements meet the condition
        #   specified by the key.
        tranDict = dict()
        for state in partition:
            # Get the transition function as a list in the tranDict key format
            # Replace the invalid transition (not in the next set) with -1
            tranFunc = tuple(map((lambda nextState: partition[nextState] if nextState is not None else -1)
                , map(lambda c: state.next.get(c), charset)))
            # Assign the new partition group index
            if (partition[state], tranFunc) in tranDict: # exist
                newPartition[state] = tranDict[(partition[state], tranFunc)]
            else: # Not exist, create a new one
                tranDict[(partition[state], tranFunc)] = i
                newPartition[state] = i
                i += 1
    # Partition done, now create the state corresponding to this partition
    # We have already known the number of groups -- "i"
    newFSM = []
    entranceState = None
    for _ in range(i):
        newFSM.append(FSMState())
    for state in partition:
        # Copy the transition table to the new FSM state list
        for tran in state.next:
            newFSM[partition[state]].next[tran] = newFSM[partition[state.next[tran]]]
        # Copy the start and accepted state to the new DFSM
        if state.accepted: # There is only one accepted state in one partition
            newFSM[partition[state]].accepted = state.accepted
            newFSM[partition[state]].acceptAction = state.acceptAction
            newFSM[partition[state]].ruleName = state.ruleName
        if state == dfsm: # Note: dfsm is the entrance state of the original DFSM
            entranceState = newFSM[partition[state]]
    return entranceState

# Return all states and transition charset in a FSM whose entrance state is fsm.
def allStatesAndCharset(fsm, states=None, charset=None):
    if states == None:
        states = set()
        charset = set()
    if fsm in states:
        return states, charset
    states.add(fsm)
    charset = charset.union(set(fsm.next.keys()))   
    for tran in fsm.next:
        states, charset = allStatesAndCharset(fsm.next[tran], states, charset)
    return states, charset

# +++++++++++++++++++++++++++++++++++

# The returned function
# it returns a list of lexical elements (lexical element name, lexeme)
def scan(fsm, selectFSM, inputStr):
    activeFSM = {fsm} # all active fsm not failed or accepted
    res = []
    lexemeBegin = 0
    forward = 0
    acceptedStack = []
    while True:
        if forward < len(inputStr):
            # Move all FSM instances to their next states
            newActiveFSM = functools.reduce(set.union, map(lambda fsm: fsm.run(inputStr[forward]), activeFSM), set())
        else:
            newActiveFSM = set()
        if not newActiveFSM: # reverted to the last accepted state if exists, raise exception otherwise
            if not acceptedStack:
                raise Exception("Lexical error at character " + str(forward))
            forward, targetState = acceptedStack.pop()
            forward += 1
            res.append((targetState.ruleName, targetState.acceptAction(inputStr[lexemeBegin:forward])))
            # reset the machine, quit if reach the end
            if forward >= len(inputStr):
                break
            lexemeBegin = forward
            activeFSM = {fsm}
        else:
            activeFSM = newActiveFSM
            # Scan for accepted states
            acceptedFSM = set(filter(lambda fsm: fsm.accepted, activeFSM))
            if acceptedFSM: # We reach at least an accepted state, push the one with highest priority into the stack
                acceptedStack.append((forward, selectFSM(acceptedFSM)))
            forward += 1
    return res

# +++++++++++++++++++++++++++++++++++++

# The data structure that describe a lexical rule.
class LexRule:
    def __init__(self, regexp, acceptAction):
        # The regular expression that describe this rule.
        self.regexp = regexp
        # The function that are executed when the FSM reaches accepted state.
        # It takes a string (which will be the string that matches the rule)
        # and return nothing.
        self.acceptAction = acceptAction

# helper function that label all states with a unique number.
def label(start, labelNum=1):
    if start.number == 0:
        start.number = labelNum
        labelNum += 1
        start.epsilonNext = set(map(lambda x: label(x, labelNum), start.epsilonNext))
        start.next = dict(map(lambda x: (x, label(start.next[x])), start.next))
    return start

# The entrance function of the module
# charSet: a dict whose key is a string and whose value is a function.
#   The key is the name of the charset that may be used in regexps.
#   The value reports if a character is in the charset.
# lexRule: An dict of the lexical rule.
#   The key is the name of this rule.it will be provided to the function that
#       resolve multiple accepted state.
#   The value is a LexRule structure that describe the rule.
# fsmSelect: A function used to resolve conflict when multiple lexical rule
# match the same string.
#   It should take a list of strings (which are the rule names that match the string)
#   and return one of the name from the list (which is the rule to be applied here).
# This function return a function that can be called and scan string.
def buildScanner(charSet, lexRules, fsmSelect):
    start = FSMState()
    # For every lexical rule, create a NFSM
    for rule in lexRules:
        lexRules[rule].regexp = parseRegex(lexRules[rule].regexp, charSet)
        start.epsilonNext.append(toNFSM(rule, lexRules[rule]))
    # run the subset construction
    FSMState.nextID = 0
    start = toDFSM(set([start]))
    # Optimize the DFSM
    FSMState.nextID = 0
    start = optimizeDFSM(start)
    return lambda inStr, start=start, fsmSelect=fsmSelect: scan(start, fsmSelect, inStr)
