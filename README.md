# ParserGen

This is a parser generator written in Python and designed as a library to be used by other Python programs. As I am currently working on another project (Mogami), this project will not be maintained for a while. 

This parser generator is top-down with a L-attributed SDD. It also includes a scanner generator. 

This library will takes in a set of lexical and grammar rules, including a list of lexical elements with their regex, a set of context-free grammar with embedded grammar actions, and several functions that determines the priority of lexical/grammar elements. The library will generate the state transition table for scanner and parser, then packs them into a returned function, which may be called to parse a string and obtain the set of attributes of the root node, along with any side effects by the grammar actions. 

There is a example JSON parser generated with this library, under /json, that demonstrate how this library works.
