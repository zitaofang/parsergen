def numAction(lexeme):
    return float(lexeme)

def strAction(lexeme):
    # Remove the head and the tail
    lexeme = lexeme[1:len(lexeme) - 1]
    # Look for \
    res = ""
    i = 0
    while i < len(lexeme):
        if lexeme[i] == '\\':
            # Replace the escaped character
            if lexeme[i + 1] == '\\' or lexeme[i + 1] == '\"' or lexeme[i + 1] == '/':
                res += lexeme[i + 1]
            elif lexeme[i + 1] == 'b':
                res += '\b'
            elif lexeme[i + 1] == 'f':
                res += '\f'
            elif lexeme[i + 1] == 'n':
                res += '\n'
            elif lexeme[i + 1] == 't':
                res += '\t'
            elif lexeme[i + 1] == 'r':
                res += '\r'
            else: # 'u'
                res += chr(int(lexeme[i + 2:i + 6], 16))
                i += 4 # skip the extra 4 char
            i += 2
        else:
            res += lexeme[i]
            i += 1
    return res

charSet = {
    "whitespace" : lambda c: c == '\u0009' or c == '\u000a' or c == '\u000d' or #
        c == '\u0020',
    "number" : lambda c : c >= '0' and c <= '9',
    "nonzero" : lambda c : c >= '1' and c <= '9',
    "hex" : lambda c : (c >= '0' and c <= '9') or (c >= 'a' and c <= 'f'),
    "ordinarystr" : lambda c : c != '\"' and c != '\\'
}

lexRule = {
    "number" : "-?(0|[nonzero]+)(.[number]+)?((e|E)(\\+|-)?[number]+)?",
    "string" : "\"([ordinarystr]|\\\\(\"|\\|/|b|f|n|r|t|u[hex][hex][hex][hex]))*\"",
    "true" : "true",
    "false" : "false",
    "null" : "null",
    "[" : "\\[",
    "]" : "\\]",
    "{" : "{",
    "}" : "}",
    ":" : ":",
    "," : ",",
    "whitespace" : "[whitespace]+"
}

acceptAction = {
    "number" : numAction,
    "string" : strAction,
    "true" : lambda _ : True,
    "false" : lambda _ : False,
    "null" : lambda _ : None,
    "[" : lambda _ : None,
    "]" : lambda _ : None,
    "{" : lambda _ : None,
    "}" : lambda _ : None,
    ":" : lambda _ : None,
    "," : lambda _ : None,
    "whitespace" : lambda _ : None
}

start = "s"

lexRuleList = list(lexRule.keys())
def fsmSelect(states):
    def ruleIndex(rule):
        i = 0
        while i < len(lexRuleList):
            if lexRuleList[i] == rule.ruleName:
                break
            i += 1
        return i
    maxInd = -1
    maxState = None
    for state in states:
        ind = ruleIndex(state)
        if ind > maxInd:
            maxInd = ind
            maxState = state
    return maxState
    
def passAction(this, value):
    this.v = value.v
    
def pairAction(this, string, _, value):
    this.v = (string.v, value.v)
    
def pairlistAction(this, _, pair):
    this.v.append(pair.v)
    return this.v
    
def pairlistPassAction(res, pairlist):
    pairlist.v = res

def pairlistNewAction(_, pairlist, __):
    pairlist.v = list()
    
def objectAction(this, _, pair, pairlist, __):
    pairlist.v.append(pair.v)
    this.v = dict(pairlist.v)
    
def elemlistAction(this, _, value):
    this.v.append(value.v)
    return this.v
    
def elemlistPassAction(res, elemlist):
    elemlist.v = res
    
def elemlistNewAction(_, elemlist, __):
    elemlist.v = list()
    
def arrayAction(this, _, value, elemlist, __):
    this.v = [value.v] + elemlist.v
    
actionList = {
    "passAction" : (passAction, None),
    "pairAction" : (pairAction, None),
    "pairlistAction" : (pairlistAction, pairlistPassAction),
    "pairlistNewAction" : (None, pairlistNewAction),
    "objectAction" : (objectAction, None),
    "elemlistAction" : (elemlistAction, elemlistPassAction),
    "elemlistNewAction" : (None, elemlistNewAction),
    "arrayAction" : (arrayAction, None)
}

CFG = {
    "s" : [["value", "passAction"]],
    "value" : [["object", "passAction"], ["array", "passAction"], ["number", "passAction"]
               , ["string", "passAction"], ["true", "passAction"], ["false", "passAction"]
               , ["null", "passAction"]],
    "pair" : [["string", ":", "value", "pairAction"]],
    "object" : [["{", "pair", "pairlistNewAction", "pairlist", "}", "objectAction"]],
    "pairlist" : [[",", "pair", "pairlistAction", "pairlist"], []],
    "array" : [["[", "value", "elemlistNewAction", "elemlist", "]", "arrayAction"]],
    "elemlist" : [[",", "value", "elemlistAction", "elemlist"], []]
}

def filterFunc(lexeme):
    return lexeme[0] != "whitespace"
