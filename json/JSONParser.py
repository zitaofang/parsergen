import grammer
import scanner
import parse

# initialization
ruleSet = dict()
for lexName in grammer.lexRule:
    ruleSet[lexName] = scanner.LexRule(grammer.lexRule[lexName], grammer.acceptAction[lexName])
s = scanner.buildScanner(grammer.charSet, ruleSet, grammer.fsmSelect)
p = parse.getParser(parse.CFG(grammer.CFG, list(grammer.lexRule.keys()), grammer.actionList, grammer.start))

def parseJSON(inputStr):
    return p(s(inputStr), grammer.filterFunc).v